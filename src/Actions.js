import axios from 'axios'

const ApiKey = "790fa1f6939d23d8984015afffbf7a75";
const BaseURL = "https://gateway.marvel.com/v1/public/";

/** 
 * Synch actions
 */

//Send to the reducer the char tab
export const setMarvelChars = (payload) => ({
	type: "SET_CHAR_TAB",
  payload: payload
})


//Set the loading state of the app
const setLoadingState = (payload) => ({
	type: "SET_LOADING_STATE",
  payload: payload
})



/** 
 * Asynch actions
 */

/**
 * Fetch the marvel chars
 * @param {*} payload { offset, order = "name", limit = 8 }
 */
export const fetchMarvelChars = (payload) => {
  return (dispatch) => {
    dispatch(setLoadingState(true));

    const { offset, order = "name", limit = 8 } = payload;

    let url = BaseURL + "characters?apikey=" + ApiKey;
    url += "&orderBy=" + order;
    url += "&limit=" + limit;
    url += "&offset=" + offset;

    return axios.get(url)
      .then((response) => {
        if(response.status === 200 && response.data.code === 200){
          dispatch(setMarvelChars(response.data.data.results));
        } else{
          console.error('[fetchMarvelChars] - Status code != 200');
        }
        dispatch(setLoadingState(false));
      })
      // A virer ? :
      .catch(function (error) {
        console.error(error);
        dispatch(setLoadingState(false));
      })
  }
}
