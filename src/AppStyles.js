export default {
  app: {
    backgroundColor: '#282c34',
    minHeight: '100vh',
    maxHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 'calc(10px + 2vmin)',
    color: 'white',
    textAlign: 'center'
  },
  charContainer:{
    display:'flex',
    flexWrap: 'wrap',
    padding: '20px 10vmin',
    overflow: 'auto',
    scrollbarWidth: 'none',
    '&::-webkit-scrollbar':{
      display: 'none'
    }
  },
  imgChar:{
    width:"20vmin",
    height:"20vmin",
  },
  nameChar:{
    fontSize: '2vmin',
    maxWidth: '20vmin',

    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow:'hidden',
  },
  margin:{
    margin: '2vmin'
  },
  isLoading:{
    cursor: 'wait'
  }
  
}