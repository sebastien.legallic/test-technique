import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AppStyles from './AppStyles';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import classnames from 'classnames'

import { fetchMarvelChars } from './Actions';

function mapStateToProps(state) {
  return {
    marvelChars: state.marvelChars,
    isLoading: state.isLoading,
  };
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentLimit: 18,
      currentOffSet: 0,
      currentOrder: "name"
    } 
    this.props.dispatch(fetchMarvelChars({ 
                                          offset: this.state.currentOffSet,
                                          order: this.state.currentOrder,
                                          limit: this.state.currentLimit
                                        }));
  }

  changeOffset = (type) => {
    const { currentOffSet, currentLimit } = this.state;
    let newOffset;
    if(type === 'right'){
      newOffset = currentOffSet + currentLimit;
    } else {
      newOffset = currentOffSet - currentLimit;
    }
    this.setState({currentOffSet: newOffset});
    
    this.props.dispatch(fetchMarvelChars({ 
      offset: newOffset,
      order: this.state.currentOrder,
      limit: this.state.currentLimit
    }));
  }

  renderChars = () =>{
    const { classes = {}, marvelChars } = this.props;
    return marvelChars.map((char, index) => {
      const thumbnailUrl = char.thumbnail.path + "." + char.thumbnail.extension;
      return <div className={classes.char} key={index}>
              <img 
                alt="thumbnail"
                src={thumbnailUrl}
                className={classes.imgChar}
              />
              <div>
                <p className={classes.nameChar}>
                 {char.name}
                </p>
              </div>
            </div>
    })
  }

  render() {
    const { classes = {}, isLoading } = this.props;
    const { currentOffSet } = this.state;
    return (
      <div className={classnames(classes.app, isLoading && classes.isLoading)}>
        Marvel's Super Heros :
        {
        <div className={classes.charContainer}>
              {this.renderChars()}
            </div>
        }
        <div>
          <Fab
            color="secondary"
            className={classnames(classes.fab, classes.margin, isLoading && classes.isLoading)}
            onClick={() => this.changeOffset('left')}
            disabled={currentOffSet === 0 || isLoading}
          >
            <Icon>keyboard_arrow_left</Icon>
          </Fab>
          <Fab
            color="secondary"
            className={classnames(classes.fab, classes.margin, isLoading && classes.isLoading)}
            onClick={() => this.changeOffset('right')}
            disabled={isLoading}
          >
            <Icon>keyboard_arrow_right</Icon>
          </Fab>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(withStyles(AppStyles)(App));

