/**    
 *
 *   Reducer : MaJ du state de l'application
 *    - trigger par les Actions
 *  
 */
export const INITIAL_STATE = {
  marvelChars: [],
  isLoading: false
};

export default function Reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'SET_CHAR_TAB':
      return  { 
                ...state,
                marvelChars: action.payload,
              };
    case 'SET_LOADING_STATE':
      return  { 
                ...state,
                isLoading: action.payload,
              };
    default:
      return state;
  }
}
